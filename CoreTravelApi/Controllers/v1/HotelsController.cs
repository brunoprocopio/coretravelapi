﻿using CoreTravelApi.Models.Infrastructure.Hotels;
using CoreTravelApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CoreTravelApi.Controllers.v1
{
    [Route("hotels")]
    public class HotelsController: Controller
    {
        public HotelsController([FromServices] HotelsWebApi service)
        {
            this.service = service;
        }

        private readonly HotelsWebApi service;

        [HttpPost]
        [Route("GetDetails")]
        public async Task<ActionResult<HotelsGetDetailsResponse>> GetDetails([FromBody]HotelsGetDetailsRequest request)
        {
            return service.GetDetails(request);
        }

        [HttpPost]
        [Route("GetList")]
        public async Task<ActionResult<HotelsGetListResponse>> GetList([FromBody]HotelsGetListRequest request)
        {
            return service.GetList(request);
        }

        [HttpPost]
        [Route("GetMetaData")]
        public async Task<ActionResult<HotelsGetMetaDataResponse>> GetMetaData()
        {
            return service.GetMetaData();
        }

        [HttpPost]
        [Route("GetPhotos")]
        public async Task<ActionResult<HotelsGetHotelPhotosResponse>> GetPhotos([FromBody]HotelsGetHotelsPhotosRequest request)
        {
            return service.GetPhotos(request);
        }

        [HttpPost]
        [Route("SearchLocations")]
        public async Task<ActionResult<dynamic>> SearchLocations([FromBody]HotelsSearchLocationsRequest request)
        {
            return service.SearchLocations(request);
        }
    }
}
