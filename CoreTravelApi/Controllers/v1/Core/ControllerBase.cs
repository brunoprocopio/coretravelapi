﻿using CoreTravelApi.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTravelApi.Controllers
{
    public abstract class CoreTravelControllerBase<T> : Controller
        where T: class
    {
        private readonly IRepositoryBase<T> repository;

        public CoreTravelControllerBase(IRepositoryBase<T> repository)
        {
            this.repository = repository;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<T>> GetById(int id)
        {
            var result = repository.GetById(id);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpGet]
        public async Task<ActionResult<List<T>>> GetAll()
        {
            var result = repository.GetAll();
            return result;
        }

        // PUT: Addresses/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, T address)
        {
            if (id == 0)
            {
                return BadRequest();
            }

            var state = repository.Update(address);

            if (state)
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        // POST: Addresses
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<T>> Create(T dto)
        {
            var id = repository.Create(dto);

            return CreatedAtAction($"Get{nameof(T)}", new { id = id }, dto);
        }

        // DELETE: Addresses/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<T>> Delete(int id)
        {
            var dto = repository.GetById(id);

            if (dto == null)
            {
                return NotFound();
            }

            repository.Delete(id);

            return dto;
        }
    }
}
