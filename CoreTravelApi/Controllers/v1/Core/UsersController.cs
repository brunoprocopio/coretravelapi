﻿using CoreTravelApi.Data.Repositories;
using CoreTravelApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreTravelApi.Controllers
{
    [Route("core/users")]
    public class CoreUsersController : CoreTravelControllerBase<UserDto>
    {
        public CoreUsersController([FromServices]UsuarioRepository viagemRepository)
            : base(viagemRepository)
        {
        }
    }
}
