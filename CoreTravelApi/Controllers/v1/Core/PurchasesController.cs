﻿using CoreTravelApi.Data.Repositories;
using CoreTravelApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreTravelApi.Controllers
{
    [Route("core/purchases")]
    [ApiController]
    public class CorePurchasesController : CoreTravelControllerBase<PurchaseDto>
    {
        public CorePurchasesController([FromServices]CompraRepository compraRepository)
            : base(compraRepository)
        {
        }
    }
}
