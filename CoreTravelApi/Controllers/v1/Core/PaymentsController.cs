﻿using CoreTravelApi.Data.Repositories;
using CoreTravelApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreTravelApi.Controllers
{
    [Route("core/payments")]
    [ApiController]
    public class CorePaymentsController : CoreTravelControllerBase<PaymentDto>
    {
        public CorePaymentsController([FromServices]PagamentoRepository pagamentoRepository)
            : base(pagamentoRepository)
        {
        }
    }
}
