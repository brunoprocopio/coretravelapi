﻿using CoreTravelApi.Data.Repositories;
using CoreTravelApi.Models.Dto;
using Microsoft.AspNetCore.Mvc;

namespace CoreTravelApi.Controllers
{
    [Route("core/addresses")]
    public class CoreAddressesController : CoreTravelControllerBase<AddressDto>
    {
        public CoreAddressesController([FromServices]EnderecoRepository enderecoRepository)
            :base(enderecoRepository)
        {
        }
    }
}
