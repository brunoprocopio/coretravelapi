﻿using CoreTravelApi.Data.Repositories;
using CoreTravelApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreTravelApi.Controllers
{
    [Route("core/flights")]
    public class CoreFlightsController : CoreTravelControllerBase<FlightDto>
    {
        public CoreFlightsController([FromServices]VooRepository vooRepository)
            : base(vooRepository)
        {
        }
    }
}
