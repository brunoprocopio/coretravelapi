﻿using CoreTravelApi.Data.Repositories;
using CoreTravelApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreTravelApi.Controllers
{
    [Route("core/travels")]
    [ApiController]
    public class CoreTravelsController : CoreTravelControllerBase<TravelDto>
    {
        public CoreTravelsController([FromServices]ViagemRepository viagemRepository)
            : base(viagemRepository)
        {
        }
    }
}
