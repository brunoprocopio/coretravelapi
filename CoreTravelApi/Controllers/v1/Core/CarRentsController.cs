﻿using CoreTravelApi.Data.Repositories;
using CoreTravelApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoreTravelApi.Controllers
{
    [Route("core/carRents")]
    [ApiController]
    public class CoreCarRentsController : CoreTravelControllerBase<CarRentDto>
    {
        public CoreCarRentsController([FromServices]AluguelCarroRepository aluguelCarrosRepository)
            : base(aluguelCarrosRepository)
        {
        }
    }
}
