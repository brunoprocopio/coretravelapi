﻿using CoreTravelApi.Models;
using CoreTravelApi.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace CoreTravelApi.Controllers
{
    [Route("core/hotels")]
    public class CoreHotelsController : CoreTravelControllerBase<HotelDto>
    {
        public CoreHotelsController([FromServices]HotelRepository hotelRepository)
            : base(hotelRepository)
        {
        }
    }
}
