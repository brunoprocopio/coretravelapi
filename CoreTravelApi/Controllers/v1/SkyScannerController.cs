﻿using CoreTravelApi.Models.Infrastructure.SkyScanner;
using CoreTravelApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreTravelApi.Controllers.v1
{
    [Route("skyscanner")]
    public class SkyScannerController : Controller
    {
        public SkyScannerController([FromServices] SkyScannerWebApi service)
        {
            this.service = service;
        }

        private readonly SkyScannerWebApi service;

        [HttpPost]
        [Route("places")]
        public async Task<ActionResult<SkyScannerPlacesResponse>> GetPlaces([FromBody] SkyScannerPlaceRequest request)
        {
            return service.GetPlaces(request.Query);
        }

        [HttpPost("flights")]
        public async Task<ActionResult<List<SkyScannerFlightResponse>>> GetFlights([FromBody] SkyScannerFlightRequest request)
        {
            return service.GetFlights(
                request.Origem,
                request.Destino,
                request.DataInicio,
                request.DataFim);
        }
    }
}
