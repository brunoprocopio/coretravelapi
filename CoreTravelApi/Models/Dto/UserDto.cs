﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoreTravelApi.Models
{
    [Table("usuario")]
    public class UserDto
    {
        public int Id { get; set; }

        public string Email { get; set; }

        [Column("senha")]
        public string Password { get; set; }

        [Column("nome")]
        public string Name { get; set; }

        [Column("data_nascimento")]
        public DateTime BirthDate { get; set; }

        [Column("telefone")]
        public string PhoneNumber { get; set; }

        [Column("endereco")]
        public int AddressId { get; set; }
    }
}
