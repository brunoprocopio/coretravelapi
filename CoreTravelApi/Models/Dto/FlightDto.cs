﻿using Dapper.Contrib.Extensions;
using System;

namespace CoreTravelApi.Models
{
    [Table("voo")]
    public class FlightDto
    {
        [Key]
        public int id { get; set; }

        public int viagem { get; set; }

        public string origem { get; set; }

        public string destino { get; set; }

        public DateTime Timestamp { get; set; }

        public int qtd_assentos { get; set; }

        public decimal preco_unitario { get; set; }
    }
}
