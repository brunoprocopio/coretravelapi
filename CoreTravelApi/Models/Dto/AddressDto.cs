﻿using Dapper.Contrib.Extensions;

namespace CoreTravelApi.Models.Dto
{
    [Table("endereco")]
    public class AddressDto
    {
        [Key]
        public int id { get; set; }

        public string cep{ get; set; }

        public string rua{ get; set; }

        public int numero{ get; set; }

        public string bairro{ get; set; }

        public string cidade{ get; set; }

        public string pais{ get; set; }
    }
}
