﻿using Dapper.Contrib.Extensions;
using System;

namespace CoreTravelApi.Models
{
    [Table("aluguel_carro")]
    public class CarRentDto
    {
        [Key]
        public int Id { get; set; }
        
        public int viagem { get; set; }
        
        public int endereco_retirada { get; set; }

        public int endereco_devolucao { get; set; }

        public DateTime data_retirada { get; set; }

        public DateTime data_devolucao { get; set; }

        public string carro { get; set; }

        public decimal preco_diaria { get; set; }
    }
}
