﻿using Dapper.Contrib.Extensions;
using System;

namespace CoreTravelApi.Models
{
    [Table("compra")]
    public class PurchaseDto
    {
        [Key]
        public int id { get; set; }

        public int usuario { get; set; }

        public int pagamento { get; set; }

        public int viagem { get; set; }

        public DateTime Timestamp { get; set; }

        public bool finalizada { get; set; }
    }
}
