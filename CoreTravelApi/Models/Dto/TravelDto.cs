﻿using Dapper.Contrib.Extensions;
using System;

namespace CoreTravelApi.Models
{
    [Table("viagem")]
    public class TravelDto
    {
        [Key]
        public int id { get; set; }

        public DateTime data_inicio { get; set; }

        public DateTime data_fim { get; set; }

        public int qtd_pessoas { get; set; }

        public int usuario { get; set; }
    }
}
