﻿using Dapper.Contrib.Extensions;
using System;

namespace CoreTravelApi.Models
{
    [Table("pagamento")]
    public class PaymentDto
    {
        [Key]
        public int id { get; set; }

        public string cartao { get; set; }

        public bool aprovado { get; set; }

        public DateTime timestamp { get; set; }

        public int usuario { get; set; }

        public UserDto User { get; set; }
    }
}
