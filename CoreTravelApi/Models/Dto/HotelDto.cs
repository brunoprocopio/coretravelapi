﻿using Dapper.Contrib.Extensions;
using System;

namespace CoreTravelApi.Models
{
    [Table("hotel")]
    public class HotelDto
    {
        [Key]
        public int id { get; set; }

        public int viagem { get; set; }

        public int endereco { get; set; }

        public DateTime checkin { get; set; }

        public DateTime checkout { get; set; }

        public string nome { get; set; }

        public string qtd_quartos { get; set; }

        public decimal preco_diaria { get; set; }
    }
}
