﻿using System.ComponentModel.DataAnnotations;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsSearchLocationsRequest
    {
        [Required]
        public string query { get; set; }

        public string? locale { get; set; }

        public HotelsSearchLocationsRequest()
        {
            locale = "pt_BR";
        }
    }
}
