﻿using System.Collections.Generic;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsGetDetailsResponse
    {
        public string result { get; set; }
        public Data data { get; set; }
        public Transportation transportation { get; set; }
        public Neighborhood neighborhood { get; set; }
    }

    public class Coordinates
    {
        public double latitude { get; set; }
        public double longitude { get; set; }

    }

    public class HotelLocation
    {
        public Coordinates coordinates { get; set; }
        public string resolvedLocation { get; set; }
        public string locationName { get; set; }

    }

    public class PdpHeader
    {
        public string hotelId { get; set; }
        public string destinationId { get; set; }
        public string pointOfSaleId { get; set; }
        public string currencyCode { get; set; }
        public string occupancyKey { get; set; }
        public HotelLocation hotelLocation { get; set; }

    }

    public class OverviewSection
    {
        public string title { get; set; }
        public string type { get; set; }
        public List<string> content { get; set; }
        public string contentType { get; set; }

    }

    public class Overview
    {
        public List<OverviewSection> overviewSections { get; set; }

    }

    public class HotelWelcomeRewards
    {
        public bool applies { get; set; }
        public string info { get; set; }

    }

    public class CurrentPrice
    {
        public string formatted { get; set; }
        public double plain { get; set; }

    }

    public class FeaturedPrice
    {
        public string beforePriceText { get; set; }
        public string afterPriceText { get; set; }
        public string pricingAvailability { get; set; }
        public string pricingTooltip { get; set; }
        public CurrentPrice currentPrice { get; set; }
        public string oldPrice { get; set; }
        public bool taxInclusiveFormatting { get; set; }
        public bool bookNowButton { get; set; }

    }

    public class MapWidget
    {
        public string staticMapUrl { get; set; }

    }

    public class PropertyDescription
    {
        public string clientToken { get; set; }
        public HotelAddress address { get; set; }
        public bool priceMatchEnabled { get; set; }
        public string name { get; set; }
        public string starRatingTitle { get; set; }
        public double starRating { get; set; }
        public FeaturedPrice featuredPrice { get; set; }
        public MapWidget mapWidget { get; set; }
        public List<string> roomTypeNames { get; set; }
        public List<string> tagline { get; set; }
        public List<string> freebies { get; set; }

    }

    public class Brands
    {
        public double scale { get; set; }
        public string formattedScale { get; set; }
        public double rating { get; set; }
        public string formattedRating { get; set; }
        public bool lowRating { get; set; }
        public string badgeText { get; set; }
        public int total { get; set; }

    }

    public class GuestReviews
    {
        public Brands brands { get; set; }
        public List<object> trustYouReviews { get; set; }

    }

    public class KeyFacts
    {
        public List<string> hotelSize { get; set; }
        public List<string> arrivingLeaving { get; set; }
        public List<string> specialCheckInInstructions { get; set; }
        public List<string> requiredAtCheckIn { get; set; }

    }

    public class Travelling
    {
        public List<string> children { get; set; }
        public List<string> pets { get; set; }
        public List<object> extraPeople { get; set; }

    }

    public class TravellingOrInternet
    {
        public Travelling travelling { get; set; }
        public List<string> internet { get; set; }

    }

    public class Transport
    {
        public List<string> transfers { get; set; }
        public List<string> parking { get; set; }
        public List<object> offsiteTransfer { get; set; }

    }

    public class TransportAndOther
    {
        public Transport transport { get; set; }
        public List<string> otherInformation { get; set; }
        public List<object> otherInclusions { get; set; }

    }

    public class AtAGlance
    {
        public KeyFacts keyFacts { get; set; }
        public TravellingOrInternet travellingOrInternet { get; set; }
        public TransportAndOther transportAndOther { get; set; }

    }

    public class ListItem
    {
        public string heading { get; set; }
        public List<string> listItems { get; set; }

    }

    public class Amenity
    {
        public string heading { get; set; }
        public List<ListItem> listItems { get; set; }

    }

    public class SmallPrint
    {
        public List<string> alternativeNames { get; set; }
        public List<object> mandatoryFees { get; set; }
        public List<string> optionalExtras { get; set; }
        public List<string> policies { get; set; }
        public bool mandatoryTaxesOrFees { get; set; }
        public bool display { get; set; }

    }

    public class Section
    {
        public string heading { get; set; }
        public string freeText { get; set; }
        public List<object> listItems { get; set; }
        public List<object> subsections { get; set; }

    }

    public class SpecialFeatures
    {
        public List<Section> sections { get; set; }

    }

    public class Miscellaneous
    {
        public string pimmsAttributes { get; set; }
        public bool showLegalInfoForStrikethroughPrices { get; set; }

    }

    public class PageInfo
    {
        public string pageType { get; set; }
        public List<object> errors { get; set; }
        public List<object> errorKeys { get; set; }

    }

    public class Body
    {
        public PdpHeader pdpHeader { get; set; }
        public Overview overview { get; set; }
        public HotelWelcomeRewards hotelWelcomeRewards { get; set; }
        public PropertyDescription propertyDescription { get; set; }
        public HotelsGuestReviews guestReviews { get; set; }
        public AtAGlance atAGlance { get; set; }
        public List<Amenity> amenities { get; set; }
        public SmallPrint smallPrint { get; set; }
        public SpecialFeatures specialFeatures { get; set; }
        public Miscellaneous miscellaneous { get; set; }
        public PageInfo pageInfo { get; set; }
        public bool trustYouReviewsCredit { get; set; }

    }

    public class PointOfSale
    {
        public string numberSeparators { get; set; }
        public string brandName { get; set; }

    }

    public class Tracking
    {
        public string pageViewBeaconUrl { get; set; }

    }

    public class Data
    {
        public Body body { get; set; }
        public HotelsCommon common { get; set; }

    }

    public class Location
    {
        public string name { get; set; }
        public string distance { get; set; }
        public string distanceInTime { get; set; }

    }

    public class TransportLocation
    {
        public string category { get; set; }
        public List<Location> locations { get; set; }
    }

    public class Transportation
    {
        public List<TransportLocation> transportLocations { get; set; }
    }

    public class Neighborhood
    {
        public string neighborhoodName { get; set; }

    }
}
