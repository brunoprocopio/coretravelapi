﻿using System.Collections.Generic;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsGetMetaDataResponse
    {
        public List<HotelsMetaData> List { get; set; }
    }
   
    public class HotelsMetaData
    {
        public string name { get; set; }
        public string posName { get; set; }
        public string hcomLocale { get; set; }
    }
}
