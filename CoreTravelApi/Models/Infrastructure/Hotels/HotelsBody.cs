﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsBody
    {
        public string header { get; set; }
        public Query query { get; set; }
        public SearchResults searchResults { get; set; }
        public SortResults sortResults { get; set; }
        public Filters filters { get; set; }
        public HotelsPointOfSale pointOfSale { get; set; }
        public Miscellaneous miscellaneous { get; set; }
        public PageInfo pageInfo { get; set; }
    }
}
