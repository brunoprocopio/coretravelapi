﻿namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsData
    {
        public HotelsBody body { get; set; }
        public HotelsCommon common { get; set; }
    }
}
