﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsGuestReviews
    {
        public double unformattedRating { get; set; }
        public string rating { get; set; }
        public int total { get; set; }
        public int scale { get; set; }
        public string badge { get; set; }
        public string badgeText { get; set; }
    }
}
