﻿using System.Collections.Generic;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Entity
    {
        public string geoId { get; set; }
        public string destinationId { get; set; }
        public string landmarkCityDestinationId { get; set; }
        public string type { get; set; }
        public string caption { get; set; }
        public string redirectPage { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string name { get; set; }

    }

    public class Suggestion
    {
        public string group { get; set; }
        public List<Entity> entities { get; set; }

    }

    public class HotelsSearchLocationsResponse
    {
        public string term { get; set; }
        public int moresuggestions { get; set; }
        public object autoSuggestInstance { get; set; }
        public string trackingID { get; set; }
        public bool misspellingfallback { get; set; }
        public List<Suggestion> suggestions { get; set; }
    }           
}
