﻿using System.ComponentModel.DataAnnotations;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsGetListRequest
    {
        [Required]
        public long destinationId { get; set; }

        [Required]
        public int pageNumber { get; set; }

        //The check-in date at hotel, formated as yyyy-MM-dd
        [Required]
        public string checkIn { get; set; }

        //The check-in date at hotel, formated as yyyy-MM-dd
        [Required]
        public string checkOut { get; set; }

        [Required]
        public int pageSize { get; set; }

        [Required]
        public int adults1 { get; set; }

        public string? currency { get; set; }

        public decimal? priceMax { get; set; }

        // BEST_SELLER|STAR_RATING_HIGHEST_FIRST|STAR_RATING_LOWEST_FIRST|DISTANCE_FROM_LANDMARK|GUEST_RATING|PRICE_HIGHEST_FIRST|PRICE
        public string? sortOrder { get; set; }

        // The age of every children separated by comma in the first room. Ex : 5,11
        public string? childer1 { get; set; }

        public HotelsGetListRequest()
        {
            currency = "BRL";
            sortOrder = "PRICE";
        }
    }
}
