﻿using System.ComponentModel.DataAnnotations;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsGetDetailsRequest
    {
        /// <summary>
        /// The value of id field that returned in GetList endpoint
        /// </summary>
        [Required]
        public long id { get; set; }

        /// <summary>
        /// The language code
        /// </summary>
        public string? locale { get; set; }

        /// <summary>
        /// The number of adults in the first room
        /// </summary>
        public int? adults1 { get; set; }

        /// <summary>
        /// The check-in date at hotel
        /// </summary>
        public string? checkIn { get; set; }

        /// <summary>
        /// The check-out date at hotel
        /// </summary>
        public string? checkOut { get; set; }

        /// <summary>
        /// The currency code
        /// </summary>
        public string? currency { get; set; }


        public HotelsGetDetailsRequest()
        {
            locale = "pt_BR";
            currency = "BRL";

        }
    }
}
