﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsPointOfSale
    {
        public string numberSeparators { get; set; }
        public string brandName { get; set; }
    }
}
