﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsAddress
    {
        public string countryName { get; set; }
        public string cityName { get; set; }
        public string postalCode { get; set; }
        public string provinceName { get; set; }
        public string addressLine1 { get; set; }
        public string countryCode { get; set; }
        public string pattern { get; set; }
        public string fullAddress { get; set; }
    }
}
