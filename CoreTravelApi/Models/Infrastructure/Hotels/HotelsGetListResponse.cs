﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsGetListResponse
    {
        public string result { get; set; }
        public HotelsData data { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Destination
    {
        public string id { get; set; }
        public string value { get; set; }
        public string resolvedLocation { get; set; }

    }

    public class Query
    {
        public Destination destination { get; set; }

    }

    public class HotelAddress
    {
        public string @streetAddress { get; set; }
        public string extendedAddress { get; set; }
        public string locality { get; set; }
        public string postalCode { get; set; }
        public string region { get; set; }
        public string countryName { get; set; }
        public string countryCode { get; set; }

    }

    public class Landmark
    {
        public string label { get; set; }
        public string distance { get; set; }

    }

    public class Price
    {
        public string current { get; set; }
        public double exactCurrent { get; set; }
        public string old { get; set; }

    }

    public class Features
    {
        public bool paymentPreference { get; set; }
        public bool noCCRequired { get; set; }

    }

    public class RatePlan
    {
        public Price price { get; set; }
        public Features features { get; set; }

    }

    public class Coordinate
    {
        public double lat { get; set; }
        public double lon { get; set; }

    }

    public class Result
    {
        public int id { get; set; }
        public string name { get; set; }
        public string thumbnailUrl { get; set; }
        public double starRating { get; set; }
        public HotelAddress address { get; set; }
        public HotelsGuestReviews guestReviews { get; set; }
        public List<Landmark> landmarks { get; set; }
        public RatePlan ratePlan { get; set; }
        public string neighbourhood { get; set; }
        public string pimmsAttributes { get; set; }
        public Coordinate coordinate { get; set; }
        public string providerType { get; set; }
        public int supplierHotelId { get; set; }
        public string vrBadge { get; set; }
        public bool isAlternative { get; set; }

    }

    public class Pagination
    {
        public int currentPage { get; set; }
        public string pageGroup { get; set; }
        public int nextPageStartIndex { get; set; }
        public int nextPageNumber { get; set; }
        public string nextPageGroup { get; set; }

    }

    public class SearchResults
    {
        public int totalCount { get; set; }
        public List<Result> results { get; set; }
        public Pagination pagination { get; set; }

    }

    public class Choice
    {
        public string label { get; set; }
        public string value { get; set; }
        public bool selected { get; set; }

    }

    public class Choice2
    {
        public string label { get; set; }
        public double id { get; set; }

    }

    public class EnhancedChoice
    {
        public string label { get; set; }
        public string itemMeta { get; set; }
        public List<Choice2> choices { get; set; }

    }

    public class Option
    {
        public string label { get; set; }
        public string itemMeta { get; set; }
        public List<Choice> choices { get; set; }
        public List<EnhancedChoice> enhancedChoices { get; set; }
        public string @selectedChoiceLabel { get; set; }

    }

    public class SortResults
    {
        public List<Option> options { get; set; }
        public double distanceOptionLandmarkId { get; set; }

    }

    public class Item
    {
        public string value { get; set; }

    }

    public class Name
    {
        public Item item { get; set; }

    }

    public class Item2
    {
        public int value { get; set; }

    }

    public class StarRating
    {
        public bool applied { get; set; }
        public List<Item2> items { get; set; }

    }

    public class Min
    {
        public double defaultValue { get; set; }

    }

    public class Max
    {
        public double defaultValue { get; set; }

    }

    public class Range
    {
        public Min min { get; set; }
        public Max max { get; set; }

    }

    public class GuestRating
    {
        public Range range { get; set; }

    }

    public class Item3
    {
        public string label { get; set; }
        public string value { get; set; }

    }

    public class Landmarks
    {
        public List<object> selectedOrder { get; set; }
        public List<Item3> items { get; set; }
        public List<object> distance { get; set; }

    }

    public class Item4
    {
        public string label { get; set; }
        public int value { get; set; }

    }

    public class Neighbourhood
    {
        public bool applied { get; set; }
        public List<Item4> items { get; set; }

    }

    public class Item5
    {
        public string label { get; set; }
        public string value { get; set; }

    }

    public class AccommodationType
    {
        public bool applied { get; set; }
        public List<Item5> items { get; set; }

    }

    public class Item6
    {
        public string label { get; set; }
        public string value { get; set; }

    }

    public class Facilities
    {
        public bool applied { get; set; }
        public List<Item6> items { get; set; }

    }

    public class Item7
    {
        public string label { get; set; }
        public string value { get; set; }

    }

    public class Accessibility
    {
        public bool applied { get; set; }
        public List<Item7> items { get; set; }

    }

    public class Item8
    {
        public string label { get; set; }
        public string value { get; set; }

    }

    public class ThemesAndTypes
    {
        public bool applied { get; set; }
        public List<Item8> items { get; set; }

    }

    public class Min2
    {
        public int defaultValue { get; set; }

    }

    public class Max2
    {
        public int defaultValue { get; set; }

    }

    public class Range2
    {
        public Min2 min { get; set; }
        public Max2 max { get; set; }
        public int increments { get; set; }

    }

    public class Price2
    {
        public string label { get; set; }
        public Range2 range { get; set; }
        public int multiplier { get; set; }

    }

    public class Item9
    {
        public string label { get; set; }
        public string value { get; set; }

    }

    public class PaymentPreference
    {
        public List<Item9> items { get; set; }

    }

    public class Item10
    {
        public string label { get; set; }
        public string value { get; set; }

    }

    public class WelcomeRewards
    {
        public string label { get; set; }
        public List<Item10> items { get; set; }

    }

    public class Filters
    {
        public bool applied { get; set; }
        public Name name { get; set; }
        public StarRating starRating { get; set; }
        public GuestRating guestRating { get; set; }
        public Landmarks landmarks { get; set; }
        public Neighbourhood neighbourhood { get; set; }
        public AccommodationType accommodationType { get; set; }
        public Facilities facilities { get; set; }
        public Accessibility accessibility { get; set; }
        public ThemesAndTypes themesAndTypes { get; set; }
        public Price2 price { get; set; }
        public PaymentPreference paymentPreference { get; set; }
        public WelcomeRewards welcomeRewards { get; set; }

    }

}
