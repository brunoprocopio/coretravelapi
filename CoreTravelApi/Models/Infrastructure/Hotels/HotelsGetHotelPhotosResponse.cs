﻿using System.Collections.Generic;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsGetHotelPhotosResponse
    {
        public int hotelId { get; set; }
        public List<HotelImage> hotelImages { get; set; }
        public List<RoomImage> roomImages { get; set; }
        public FeaturedImageTrackingDetails featuredImageTrackingDetails { get; set; }
        public PropertyImageTrackingDetails propertyImageTrackingDetails { get; set; }
    }

    public class Size
    {
        public int type { get; set; }
        public string suffix { get; set; }

    }

    public class TrackingDetails
    {
        public string version { get; set; }
        public string @namespace { get; set; }
        public string algorithmName { get; set; }

    }
    public class HotelImage
    {
        public string baseUrl { get; set; }
        public int imageId { get; set; }
        public string mediaGUID { get; set; }
        public List<Size> sizes { get; set; }
        public TrackingDetails trackingDetails { get; set; }

    }

    public class Size2
    {
        public int type { get; set; }
        public string suffix { get; set; }

    }

    public class Image
    {
        public string baseUrl { get; set; }
        public int imageId { get; set; }
        public string mediaGUID { get; set; }
        public List<Size2> sizes { get; set; }

    }

    public class RoomImage
    {
        public int roomId { get; set; }
        public List<Image> images { get; set; }

    }

    public class FeaturedImageTrackingDetails
    {
        public string version { get; set; }
        public string @namespace { get; set; }
        public string algorithmName { get; set; }

    }
    public class PropertyImageTrackingDetails
    {
        public string version { get; set; }
        public string @namespace { get; set; }
        public string algorithmName { get; set; }

    }
}