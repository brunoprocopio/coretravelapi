﻿using System.ComponentModel.DataAnnotations;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsGetHotelsPhotosRequest
    {
        [Required]
        public long id { get; set; }
    }
}
