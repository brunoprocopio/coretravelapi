﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTravelApi.Models.Infrastructure.Hotels
{
    public class HotelsMiscellaneous
    {
        public string pageViewBeaconUrl { get; set; }
    }
}
