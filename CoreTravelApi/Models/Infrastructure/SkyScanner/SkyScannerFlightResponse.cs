﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTravelApi.Models.Infrastructure.SkyScanner
{
    public class SkyScannerFlightResponse
    {
        public string Companhia { get; set; }
        public double Preco { get; set; }
        public List<SkyScannerFlight> Ida { get; set; }
        public List<SkyScannerFlight> Volta { get; set; }

        public SkyScannerFlightResponse()
        {
            Ida = new List<SkyScannerFlight>();
            Volta = new List<SkyScannerFlight>();
            Preco = 0;
        }

        public SkyScannerFlightResponse(string companhia)
        {
            Ida = new List<SkyScannerFlight>();
            Volta = new List<SkyScannerFlight>();
            Preco = 0;
            Companhia = companhia;
        }

        public SkyScannerFlightResponse(string companhia, double preco, List<SkyScannerFlight> ida)
        {
            Companhia = companhia;
            Preco = preco;
            Ida = ida;
        }

        public SkyScannerFlightResponse(string companhia, double preco, List<SkyScannerFlight> ida, List<SkyScannerFlight> volta)
        {
            Companhia = companhia;
            Preco = preco;
            Ida = ida;
            Volta = volta;
        }
    }
}
