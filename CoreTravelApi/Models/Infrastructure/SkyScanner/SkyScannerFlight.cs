﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTravelApi.Models.Infrastructure.SkyScanner
{
    public class SkyScannerFlight
    {
        public string AeroportoOrigem { get; set; }
        public string AeroportoDestino { get; set; }
        public string CidadeOrigem { get; set; }
        public string CidadeDestino { get; set; }
        public string PaisOrigem { get; set; }
        public string PaisDestino { get; set; }
        public double Preco { get; set; }
        public bool VooDireto { get; set; }
        public List<string> Companhias { get; set; }
        public DateTime DataPartida { get; set; }

        public SkyScannerFlight()
        {
            Companhias = new List<string>();
        }
    }
}
