﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTravelApi.Models.Infrastructure.SkyScanner
{
    public class SkyScannerPlacesResponse
    {
        public List<Place> Airports { get; set; }
        public List<Place> Places { get; set; }

        public SkyScannerPlacesResponse()
        {
            Airports = new List<Place>();
            Places = new List<Place>();
        }
    }
}
