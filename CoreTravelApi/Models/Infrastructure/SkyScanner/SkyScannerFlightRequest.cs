﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreTravelApi.Models.Infrastructure.SkyScanner
{
    public class SkyScannerFlightRequest
    {
        public string Origem { get; set; }
        public string Destino { get; set; }
        public string DataInicio { get; set; }
        public string DataFim { get; set; }
    }
}
