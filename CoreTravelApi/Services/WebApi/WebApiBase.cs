﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoreTravelApi.Services.WebApi
{
    public class WebApiBase
    {
        protected string BaseUrl { get; set; }

        protected Dictionary<string, string> Headers = new Dictionary<string, string>();

        public T Get<T>(string resource, object objToQuery = null)
        {
            var url = BaseUrl + resource + GetQueryString(objToQuery);

            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            // adiciona os headers
            foreach (var header in Headers)
            {
                request.AddHeader(header.Key, header.Value);
            }

            IRestResponse response = client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            } 
            else
            {
                throw new Exception(string.Concat("Erro ao chamar o serviço", resource));
            }
        }

        private string GetQueryString(object obj)
        {
            if (obj == null) return "";
            var properties = from p in obj.GetType().GetProperties()
                             where p.GetValue(obj, null) != null
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());

            return "?" + String.Join("&", properties.ToArray());
        }
    }
}
