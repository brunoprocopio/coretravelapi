﻿using CoreTravelApi.Attributes;
using CoreTravelApi.Models.Infrastructure.SkyScanner;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;

namespace CoreTravelApi.Services
{
    [Transient]
    public class SkyScannerWebApi
    {
        private readonly string host;
        private readonly string key;

        public SkyScannerWebApi([FromServices] IConfiguration configuration)
        {
            host = configuration["SkyScanner:Host"];
            key = configuration["SkyScanner:Key"];
        }

        public SkyScannerPlacesResponse GetPlaces(string query)
        {
            var client = new RestClient("https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/autosuggest/v1.0/BR/BRL/pt-BR/?query=" +
                query);
            var request = new RestRequest(Method.GET);
            request.AddHeader("x-rapidapi-host", host);
            request.AddHeader("x-rapidapi-key", key);
            IRestResponse response = client.Execute(request);

            SkyScannerPlaceResponse objResponse = JsonConvert.DeserializeObject<SkyScannerPlaceResponse>(response.Content);

            return SplitPlaces(objResponse);
        }

        private SkyScannerPlacesResponse SplitPlaces(SkyScannerPlaceResponse places)
        {
            SkyScannerPlacesResponse response = new SkyScannerPlacesResponse();
            foreach (Place p in places.Places)
            {
                if (p.PlaceId.Equals(p.CountryId) || p.PlaceId.Equals(p.CityId))
                    response.Places.Add(p);
                else
                    response.Airports.Add(p);
            }

            return response;
        }

        public List<SkyScannerFlightResponse> GetFlights(
            string origin,
            string destination,
            string outboundPartialDate,
            string inboundPartialDate
            )
        {
            // buscar voos de ida
            List<SkyScannerFlight> voosIda = GetFlights(origin, destination, outboundPartialDate);

            Dictionary<string, SkyScannerFlightResponse> dict = new Dictionary<string, SkyScannerFlightResponse>();

            foreach (SkyScannerFlight f in voosIda)
            {
                foreach (string c in f.Companhias)
                {
                    if (!dict.ContainsKey(c))
                    {
                        dict.Add(c, new SkyScannerFlightResponse(c));
                    }

                    dict[c].Ida.Add(f);
                    dict[c].Preco = f.Preco + dict[c].Preco;
                }
            }

            if (inboundPartialDate != null)
            {
                List<SkyScannerFlight> voosVolta = GetFlights(origin, destination, inboundPartialDate);
                foreach (SkyScannerFlight f in voosVolta)
                {
                    foreach (string c in f.Companhias)
                    {
                        if (!dict.ContainsKey(c))
                        {
                            dict.Add(c, new SkyScannerFlightResponse(c));
                        }

                        dict[c].Volta.Add(f);
                        dict[c].Preco = f.Preco + dict[c].Preco;
                    }
                }
            }

            List<SkyScannerFlightResponse> response = new List<SkyScannerFlightResponse>();

            foreach (KeyValuePair<string, SkyScannerFlightResponse> entry in dict)
            {
                response.Add(entry.Value);
            }

            return response;
        }

        private List<SkyScannerFlight> GetFlights(
            string origin,
            string destination,
            string dateStr)
        {
            DateTime date = GetDateTime(dateStr);
            RestClient client = new RestClient("https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browseroutes/v1.0/BR/BRL/pt-BR/" +
                    origin +
                    "/" +
                    destination +
                    "/" +
                    dateStr);

            var request = new RestRequest(Method.GET);
            request.AddHeader("x-rapidapi-host", host);
            request.AddHeader("x-rapidapi-key", key);
            IRestResponse response = client.Execute(request);

            SkyScannerRouteResponse objResponse = JsonConvert.DeserializeObject<SkyScannerRouteResponse>(response.Content);
            return MapFlights(objResponse, date);
        }

        private List<SkyScannerFlight> MapFlights(SkyScannerRouteResponse obj, DateTime dt)
        {
            Dictionary<int, RoutePlace> places = GetPlacesDictionary(obj.Places);
            Dictionary<int, Carrier> carriers = GetCarriersDictionary(obj.Carriers);
            List<SkyScannerFlight> flights = new List<SkyScannerFlight>();

            foreach (Quote q in obj.Quotes)
            {
                if (IsSameDate(dt, q.OutboundLeg.DepartureDate))
                {
                    SkyScannerFlight flight = new SkyScannerFlight();

                    flight.AeroportoDestino = places[q.OutboundLeg.DestinationId].Name;
                    flight.AeroportoOrigem = places[q.OutboundLeg.OriginId].Name;
                    flight.CidadeDestino = places[q.OutboundLeg.DestinationId].CityName;
                    flight.CidadeOrigem = places[q.OutboundLeg.OriginId].CityName;

                    foreach (int id in q.OutboundLeg.CarrierIds)
                        flight.Companhias.Add(carriers[id].Name);

                    flight.DataPartida = q.OutboundLeg.DepartureDate;
                    flight.VooDireto = q.Direct;
                    flight.PaisDestino = places[q.OutboundLeg.DestinationId].CountryName;
                    flight.PaisOrigem = places[q.OutboundLeg.OriginId].CountryName;
                    flight.Preco = q.MinPrice;

                    flights.Add(flight);
                }
            }
            return flights;
        }

        private DateTime GetDateTime(string date)
        {
            return new DateTime(
                int.Parse(date.Substring(0, 4)),
                int.Parse(date.Substring(5, 2)),
                int.Parse(date.Substring(8, 2)));
        }

        private bool IsSameDate(DateTime x, DateTime y)
        {
            return x.Year == y.Year && x.Month == y.Month && x.Day == y.Day;
        }

        private Dictionary<int, RoutePlace> GetPlacesDictionary(List<RoutePlace> places)
        {
            Dictionary<int, RoutePlace> dict = new Dictionary<int, RoutePlace>();

            foreach (RoutePlace p in places)
                dict.Add(p.PlaceId, p);

            return dict;
        }

        private Dictionary<int, Carrier> GetCarriersDictionary(List<Carrier> carriers)
        {
            Dictionary<int, Carrier> dict = new Dictionary<int, Carrier>();

            foreach (Carrier c in carriers)
                dict.Add(c.CarrierId, c);

            return dict;
        }
    }
}
