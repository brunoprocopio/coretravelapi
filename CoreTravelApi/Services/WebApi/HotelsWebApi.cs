﻿using CoreTravelApi.Attributes;
using CoreTravelApi.Models.Infrastructure.Hotels;
using CoreTravelApi.Services.WebApi;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace CoreTravelApi.Services
{
    [TransientAttribute]
    public class HotelsWebApi : WebApiBase
    {
        public HotelsWebApi([FromServices] IConfiguration configuration) : base()
        {
            BaseUrl = configuration["ExternalWebApis:HotelsApi:BaseUrl"];
            Headers.Add("x-rapidapi-host", configuration["ExternalWebApis:HotelsApi:Host"]);
            Headers.Add("x-rapidapi-key", configuration["ExternalWebApis:HotelsApi:Token"]);
        }

        public HotelsSearchLocationsResponse SearchLocations(HotelsSearchLocationsRequest request)
        {
            var resource = "/locations/search";
            return Get<HotelsSearchLocationsResponse>(resource, request);
        }

        public HotelsGetHotelPhotosResponse GetPhotos(HotelsGetHotelsPhotosRequest request)
        {
            var resource = "/properties/get-hotel-photos";
            return Get<HotelsGetHotelPhotosResponse>(resource, request);
        }

        public HotelsGetDetailsResponse GetDetails(HotelsGetDetailsRequest request)
        {
            var resource = "/properties/get-details";
            return Get<HotelsGetDetailsResponse>(resource, request);
        }

        public HotelsGetMetaDataResponse GetMetaData()
        {
            var resource = "/get-meta-data";
            var metaData = Get<List<HotelsMetaData>>(resource);
            var ret = new HotelsGetMetaDataResponse();
            ret.List = metaData;
            return ret;
        }

        public HotelsGetListResponse GetList(HotelsGetListRequest request)
        {
            var resource = "/properties/list";
            return Get<HotelsGetListResponse>(resource, request);
        }
    }
}
