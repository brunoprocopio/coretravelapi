using CoreTravelApi.Attributes;
using CoreTravelApi.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CoreTravelApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "EasyTrip API",
                    Description = "Core API for EasyTrip system",
                    //TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Raul Oliveira",
                        Email = "raul.soares.oliveira@usp.br",
                        //Url = new Uri("https://twitter.com/spboyer"),
                    },
                    //License = new OpenApiLicense
                    //{
                    //    Name = "Use under LICX",
                    //    Url = new Uri("https://example.com/license"),
                    //}
                });
                //// Set the comments path for the Swagger JSON and UI.
                //var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                //var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //c.IncludeXmlComments(xmlPath);
            });

            services.AddControllers();
            ConfigureAutoDI(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ExceptionHandler>();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                //c.RoutePrefix = string.Empty;
            });

            app.UseCors(opt =>
            {
                opt.AllowAnyOrigin();
                opt.AllowAnyMethod();
                opt.AllowAnyHeader();
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        public void ConfigureAutoDI(IServiceCollection services)
        {
            var assembly = typeof(Startup).Assembly.ExportedTypes;
            var types = assembly
                .Where(x => !x.IsAbstract && !x.IsInterface);

            foreach (var type in types)
            {
                if (type.GetCustomAttributes(typeof(SingletonAttribute), true).Length > 0)
                {
                    if (type.GetInterface($"I{type.Name}") != null)
                    {
                        services.AddSingleton(type.GetInterface($"I{type.Name}"), type);
                    }
                    else
                    {
                        services.AddSingleton(type);
                    }
                }
                else if (type.GetCustomAttributes(typeof(ScopedAttribute), true).Length > 0)
                {
                    if (type.GetInterface($"I{type.Name}") != null)
                    {
                        services.AddScoped(type.GetInterface($"I{type.Name}"), type);
                    }
                    else
                    {
                        services.AddScoped(type);
                    }
                }
                else if (type.GetCustomAttributes(typeof(TransientAttribute), true).Length > 0)
                {
                    if (type.GetInterface($"I{type.Name}") != null)
                    {
                        services.AddTransient(type.GetInterface($"I{type.Name}"), type);
                    }
                    else
                    {
                        services.AddTransient(type);
                    }
                }
            }
        }
    }
}
