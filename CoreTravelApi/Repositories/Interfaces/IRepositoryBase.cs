﻿using System.Collections.Generic;

namespace CoreTravelApi.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        int Create(T dto);
        bool Delete(int id);
        List<T> GetAll();
        T GetById(int id);
        bool Update(T dto);
    }
}