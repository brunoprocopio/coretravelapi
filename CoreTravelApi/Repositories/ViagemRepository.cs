﻿using CoreTravelApi.Attributes;
using CoreTravelApi.Models;
using CoreTravelApi.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CoreTravelApi.Data.Repositories
{
    [Transient]
    public class ViagemRepository : RepositoryBase<TravelDto>
    {
        public ViagemRepository([FromServices] IConfiguration configuration) : base(configuration)
        {
        }
    }
}