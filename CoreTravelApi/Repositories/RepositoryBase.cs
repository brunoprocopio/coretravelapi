﻿using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CoreTravelApi.Repositories
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T>
        where T : class
    {
        private readonly string connName;

        public RepositoryBase([FromServices]IConfiguration configuration)
        {
            connName = configuration["ConnectionStrings:DsidDatabase"];
        }

        public T GetById(int id)
        {
            using var conn = new SqlConnection(connName);
            return conn.Get<T>(id);
        }

        public List<T> GetAll()
        {
            using var conn = new SqlConnection(connName);
            return conn.GetAll<T>().ToList();
        }

        public bool Update(T dto)
        {
            using var conn = new SqlConnection(connName);
            return conn.Update<T>(dto);
        }

        public bool Delete(int id)
        {
            using var conn = new SqlConnection(connName);
            return conn.Delete<T>(GetById(id));
        }

        public int Create(T dto)
        {
            using var conn = new SqlConnection(connName);
            return Convert.ToInt32(conn.Insert<T>(dto));
        }
    }
}
