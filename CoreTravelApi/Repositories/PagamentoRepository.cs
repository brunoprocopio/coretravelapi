﻿using CoreTravelApi.Attributes;
using CoreTravelApi.Models;
using CoreTravelApi.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CoreTravelApi.Data.Repositories
{
    [Transient]
    public class PagamentoRepository : RepositoryBase<PaymentDto>
    {
        public PagamentoRepository([FromServices] IConfiguration configuration) : base(configuration)
        {
        }
    }
}
