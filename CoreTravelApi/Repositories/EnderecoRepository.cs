﻿using CoreTravelApi.Attributes;
using CoreTravelApi.Models.Dto;
using CoreTravelApi.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CoreTravelApi.Data.Repositories
{
    [Transient]
    public class EnderecoRepository : RepositoryBase<AddressDto>
    {
        public EnderecoRepository([FromServices] IConfiguration configuration) : base(configuration)
        {
        }
    }
}
