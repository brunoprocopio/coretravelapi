﻿using CoreTravelApi.Attributes;
using CoreTravelApi.Models;
using CoreTravelApi.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CoreTravelApi.Data.Repositories
{
    [Transient]
    public class AluguelCarroRepository : RepositoryBase<CarRentDto>
    {
        public AluguelCarroRepository([FromServices] IConfiguration configuration) : base(configuration)
        {
        }
    }
}
