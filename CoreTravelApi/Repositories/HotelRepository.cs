﻿using CoreTravelApi.Attributes;
using CoreTravelApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CoreTravelApi.Repositories
{
    [Transient]
    public class HotelRepository : RepositoryBase<HotelDto>
    {
        public HotelRepository([FromServices] IConfiguration configuration) : base(configuration)
        {
        }
    }
}